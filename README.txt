
-- SUMMARY --

VBO Add To Cart module provides the feature of add multiple products into shopping cart.

This module will work with VBO and Drupal Commerce.

you need to enable this module and configure steps which are mentioned below.

For a full description of the module, visit the project page:
  https://www.drupal.org/project/vbo_add_to_cart

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/vbo_add_to_cart


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* enable this module same as other drupal modules (VBO and Drupal Commerce must be enabled)
* Edit your product list view
* Add "field: Global: Views bulk operations" field
* Open SELECTED ACTIONS
* Check "VBO add to cart action" and give your label
* save field and save view


-- CONTACT --

Current maintainers:
* Pradeep Kumar Singh (druprad) - https://www.drupal.org/u/druprad
* Vishal D. Sheladiya (vishalsheladiya) - http://drupal.org/user/2915771

