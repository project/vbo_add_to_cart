<?php

namespace Drupal\vbo_add_to_cart\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsPreconfigurationInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\commerce\commerce_product;
use Drupal\commerce;
use Drupal\commerce_cart;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\StringTranslation\StringTranslationTrait;
/**
 * An example action covering most of the possible options.
 *
 * If type is left empty, action will be selectable for all
 * entity types.
 *
 * @Action(
 *   id = "vbo_add_to_cart",
 *   label = @Translation("VBO add to cart action"),
 *   type = "",
 *   confirm = FALSE,
 * )
 */

class ViewsBulkAddToCartAction extends ViewsBulkOperationsActionBase {
  use StringTranslationTrait;
  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /*
      * All config resides in $this->configuration.
      * Passed view rows will be available in $this->context.
      * Data about the view used to select results and optionally
      * the batch context are available in $this->context or externally
      * through the public getContext() method.
      * The entire ViewExecutable object  with selected result
      * rows is available in $this->view or externally through
      * the public getView() method.
      */

    if (!isset($this->context['sandbox']['counter'])) {
      $this->context['sandbox']['counter'] = 0;
    }

    if (!empty($this->context['list'])) {
      foreach ($this->context['list'] as $key=>$product_id) {
        $productId = $product_id[0];
        $productObj = Product::load($productId);
        $product_variation_id = $productObj->get('variations')->getValue()[0]['target_id'];

        $storeId = $productObj->get('stores')->getValue()[0]['target_id'];
        $variationobj = \Drupal::entityTypeManager()->getStorage('commerce_product_variation')->load($product_variation_id);
        $store = \Drupal::entityTypeManager()->getStorage('commerce_store')->load($storeId);

        $uid = \Drupal::currentUser()->id();
        $connection = \Drupal::database();
        $result = $connection->query("SELECT co.order_id, co.uid,oi.purchased_entity,oi.title,oi.quantity FROM commerce_order co JOIN commerce_order_item oi ON oi.order_id = co.order_id WHERE co.cart = 1 AND co.state = 'draft' AND co.uid = :user_id AND oi.purchased_entity = :product_variation_id", [':user_id' => $uid, ':product_variation_id' => $product_variation_id])->fetchAssoc();

        if (empty($result)) {
          $entity_manager = \Drupal::entityManager();
          $cart_provider = \Drupal::service('commerce_cart.cart_provider');
          $cart_manager = \Drupal::service('commerce_cart.cart_manager');
          $cart = $cart_provider->getCart('default', $store);

          if (!$cart) {
            $cart = $cart_provider->createCart('default', $store);
          }

          $order_item = $entity_manager->getStorage('commerce_order_item')->create(array(
            'type' => 'default',
            'purchased_entity' => (string) $product_variation_id,
            'quantity' => 1,
            'unit_price' => $variationobj->getPrice(),
          ));
          $order_item->setQuantity('1');
          $order_item->save();
          $cart_manager->addOrderItem($cart, $order_item);
        }
        else {
          \Drupal::logger('product_cart_exist')->warning('product_variation_id ' . $productId . ' already exist for this user ' . $uid);
        }
      }
    }

    $this->context['sandbox']['counter']++;
    return \Drupal::messenger()->addMessage($this->t('Items are added to cart successfully.'));
  }

  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'node') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    return TRUE;
  }
}
